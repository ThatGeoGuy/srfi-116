# SRFI-116 Changelog

## 1.8.1
### Removed
- Extraneous test file in the project root.

## 1.8.0
### Changed
- CHICKEN-4 support (and extraneous files) removed from repo.

## 1.7
### Added
- Adds SRFI-128 dependency for comparators

## 1.6
### Changed
- Updates for PFN #4

## 1.5
### Added
- CHICKEN-5 support

## 1.4
### Fixed
- Fixes from the mailing list for `ilist=`, `ievery`, and exports from Peter Lane.

## 1.3
### Fixed
- Removed hardcoded `.so` extension in setup file (CHICKEN-4)

## 1.2
### Added
- Standard README.org to SRFI repo

## 1.1
### Changed
- Packaged egg without extraneous files

## 1.0
### Added
- First release
