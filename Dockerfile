# Dockerfile for SRFI-116
#
# Instructions:
#
#    docker build -f Dockerfile -t registry.gitlab.com/thatgeoguy/srfi-116:latest .
#    docker push registry.gitlab.com/thatgeoguy/srfi-116:latest

#    docker build -f Dockerfile -t registry.gitlab.com/thatgeoguy/srfi-116:VERSION .
#    docker push registry.gitlab.com/thatgeoguy/srfi-116:VERSION

FROM alpine:latest
RUN set -eux; \
    # Install chicken
    apk add chicken; \
    #
    # Install beaker (chicken-clean, chicken-lint)
    chicken-install beaker; \
    #
    # Dependencies for srfi-116
    chicken-install r7rs srfi-99 srfi-128; \
    #
    # Test dependencies for srfi-116
    chicken-install test;

LABEL org.opencontainers.image.authors="Jeremy Steward <jeremy@thatgeoguy.ca>"
LABEL org.opencontainers.image.version="0.1.0"
LABEL org.opencontainers.image.licenses="BSD"
LABEL org.opencontainers.image.title="SRFI-116"
LABEL org.opencontainers.image.description="Docker image for SRFI-116"

CMD ["bash"]
